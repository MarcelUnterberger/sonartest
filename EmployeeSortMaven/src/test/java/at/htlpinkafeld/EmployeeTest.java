/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *
 * @author petro
 */
public class EmployeeTest {
    Employee emp;
    public EmployeeTest() {
        emp = new Employee("hans", "if", 1345);
    }
    

    /**
     * Test of compareTo method, of class Employee.
     */
    @Test
    public void testCompareTo() {
        Employee retVal;
        retVal = new Employee("hans", "if", 1345);
        assertEquals(0, emp.compareTo(retVal));
        retVal = new Employee("hans", "", 111);
        assertEquals(false, emp.compareTo(retVal) == 0);
        retVal = new Employee("hans", "if", 1344);
        assertEquals(100, emp.compareTo(retVal));
    }
    
}

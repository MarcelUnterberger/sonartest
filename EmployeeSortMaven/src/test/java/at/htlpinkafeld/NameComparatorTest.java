/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *
 * @author petro
 */
public class NameComparatorTest {
    
    NameComparator comp;
    

    /**
     * Test of compare method, of class NameComparator.
     */
    @Test
    public void testCompare() {
        comp = new NameComparator();
        
        Employee e1 = new Employee("Hans", "IF", 1234);
        Employee e2 = e1;
        assertEquals(0, comp.compare(e1,e2));
        e2 = new Employee("Hant", "IF", 1234);
        assertEquals(-1, comp.compare(e1, e2));
        e2 = new Employee("Hans", "IF", 1233);
        assertEquals(100, comp.compare(e1,e2));
        
        
        
    }
    
}

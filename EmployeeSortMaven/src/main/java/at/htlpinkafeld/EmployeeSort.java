/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 *
 * @author petro
 */
public class EmployeeSort {
    public static void main(String[] args){
        Employee[] eList = new Employee[4];
        eList[0] = new Employee("Hans", "IF", 1500);
        eList[1] = new Employee("Terry", "BT", 1600);
        eList[2] = new Employee("Adam", "GT",1440);
        eList[3] = new Employee("Gunther", "IF", 1440);
        Scanner scanner = new Scanner(System.in);
        
        boolean run = true;
        boolean ascending = true;
        String choice;
        String eName;
        String eDept;
        double eSal;
        while(run){
            System.out.println("e) Employee Input");
            System.out.println("n) sort by name");
            System.out.println("d) sort by department");
            System.out.println("s) sort by salary");
            if(ascending) System.out.println("t) toggle ASCENDING/descending");
            else System.out.println("t) toggle ascending/DESCENDING");
            System.out.println("o) Exit the menu");
            System.out.println("");
            System.out.printf("your choice: ");
            Comparator<Employee> comp = new NameComparator();
            choice = scanner.nextLine();
            switch(choice){
                case "e" :
                System.out.printf("Name: ");
                eName = scanner.nextLine();
                System.out.print("Department: ");
                eDept = scanner.nextLine();
                System.out.printf("Salary: ");
                eSal = Double.parseDouble(scanner.nextLine());
                eList = addEmployee(eList, new Employee(eName, eDept, eSal));
                break;
                case "n" : break;
                case "d" : comp = new DepartmentComparator();
                break;
                case "s" : comp = new SalaryComparator();
                break;
                case "t" : ascending = !ascending;
                break;
                case "o" : return;
                default: System.out.println("Wrong input");
            }
            if(choice.charAt(0) != 'e' && choice.charAt(0) != 't'){
                if(ascending){
                    Arrays.sort(eList, comp);
                }else{
                    comp = Collections.reverseOrder();
                    Arrays.sort(eList, comp);
                }
            }
            for(Employee ele: eList){
                System.out.println(ele);
            }
             
        }
    }
    public static Employee[] addEmployee(Employee[] eList, Employee emp){
        Employee[] tmp = new Employee[eList.length+1];
        System.arraycopy(eList, 0, tmp, 0, eList.length);
        tmp[eList.length] = emp;
        eList = tmp;
        return eList;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import java.util.Objects;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcel
 */
public class BankAccountTest {
    
    private BankAccount instance;
    public BankAccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new BankAccount();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class BankAccount.
     */
    @Test
    public void testSetInterestRate() {
        instance.setInterestRate(15);
        assertEquals(15,instance.interestRate,0.001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeInterestRate() {
        final double NEGATIVE_AMOUNT = -2.00;
        instance.setInterestRate(NEGATIVE_AMOUNT); 
    }
    
    @Test 
    public void testSetTransactionFee() {
        instance.setTransactionFee(30);
        assertEquals(30,instance.transactionFee,0.001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeTransactionFee() {
        final double NEGATIVE_FEE = -2.00;
        instance.setTransactionFee(NEGATIVE_FEE);
    }
    
    @Test
    public void testOpenAccount() {
        instance.openAccount(5, "Marcel");
        String name= "Marcel";
        int num=5;
        assertEquals(instance.accountOwner,name);
        assertEquals(instance.accountNumber,num);
        
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void negativeAccNum() {
        final int NEGATIVE_NUM = -2;
        instance.openAccount(NEGATIVE_NUM, "");
    }
    
    @Test
    public void testDeposit() {
        instance.balance=0;
        instance.deposit(5);
        double dep= 5.00;
        assertEquals(instance.balance,dep,0.001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeDeposit() {
        final double NEGATIVE_NUM = -2.00;
        instance.deposit(NEGATIVE_NUM);
    }
    
    @Test
    public void testWithdraw() {
        instance.deposit(7.0);
        System.out.println(instance.balance);
        assertTrue(instance.withdraw(5.0));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeWithdraw() {
        final double NEG_NUM = -2.00;
        instance.withdraw(NEG_NUM);
    }
    
    @Test
    public void testMonProc() {
        instance.deposit(300.00);
        instance.setTransactionFee(0.50);
        instance.setInterestRate(0.15);
        instance.numberOfTransactions=3;
        instance.monthlyProcess();
        double test= (300-0.50*3)*(1+instance.interestRate);
        assertEquals(instance.balance,test,0.001);
    }

    
    
    
}

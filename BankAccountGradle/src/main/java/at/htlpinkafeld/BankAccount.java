/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Marcel
 */
public class BankAccount {
    
    protected int accountNumber;
    protected String accountOwner;
    protected double balance;
    protected double interestRate;
    protected int numberOfTransactions;
    protected double transactionFee;
    private Object bugTest = null;
    
    public BankAccount() {
        this.balance = 0;
        if(bugTest != null) {
            int i = 0;
        }
    }

    public void setInterestRate(double interestRatePercent) {
        //if (bugTest != null) {
            if (interestRatePercent >= 0) {
                this.interestRate = interestRatePercent;
            } else {
                throw new IllegalArgumentException();
            }
        //}

    }

    public void setTransactionFee(double transactionFee) {
        //while (true) {
            if (transactionFee >= 0) {
                this.transactionFee = transactionFee;
            } else {
                throw new IllegalArgumentException();
            }
        //}

    }

    public void openAccount(int number, String name) {
        if (number > 0) {
            this.accountNumber = number;
            this.accountOwner = name;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void deposit(double amount) {
        if(amount>0) {
            this.balance+=amount;
            this.bugTest = null;
        } else {
            throw new IllegalArgumentException();
        }
        
    }
    
    public void doDeposit(double amount) {
        if(amount>0) {
            this.balance+=amount;
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    public boolean withdraw(double amount) {
        this.numberOfTransactions--;
        if(amount <= 0) {
            throw new IllegalArgumentException();
        }
        if (this.balance >= amount) {
            this.balance -= amount;
            return true;
        } else {
            return false;
        }
    }
    
    public void showAccount() {
        System.out.println("Bank Account Number: " + this.accountNumber);
        System.out.println("Owner: " + this.accountOwner);
    }
    
    public void monthlyProcess() {
        this.balance -= this.numberOfTransactions * this.transactionFee;
        this.balance += this.calculateInterest();
    }
    public double calculateInterest() {
        return this.balance * (1 + this.interestRate) - this.balance;
    }

    /*@Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.accountNumber;
        hash = 23 * hash + Objects.hashCode(this.accountOwner);
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.balance) ^ (Double.doubleToLongBits(this.balance) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.interestRate) ^ (Double.doubleToLongBits(this.interestRate) >>> 32));
        hash = 23 * hash + this.numberOfTransactions;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.transactionFee) ^ (Double.doubleToLongBits(this.transactionFee) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BankAccount other = (BankAccount) obj;
        if (this.accountNumber != other.accountNumber) {
            return false;
        }
        if (Double.doubleToLongBits(this.balance) != Double.doubleToLongBits(other.balance)) {
            return false;
        }
        if (Double.doubleToLongBits(this.interestRate) != Double.doubleToLongBits(other.interestRate)) {
            return false;
        }
        if (this.numberOfTransactions != other.numberOfTransactions) {
            return false;
        }
        if (Double.doubleToLongBits(this.transactionFee) != Double.doubleToLongBits(other.transactionFee)) {
            return false;
        }
        if (!Objects.equals(this.accountOwner, other.accountOwner)) {
            return false;
        }
        return true;
    }*/

    
}

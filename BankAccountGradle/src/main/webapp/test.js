export default function AJAXBase() {

}

AJAXBase.prototype.loadDoc = function(method, url, callBackFunction, content) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.send(content);

    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                console.log("xhr Request successful");
                callBackFunction(xhr);
            } else {
                console.log("xhr Request failed");
            }
        }
    }
}
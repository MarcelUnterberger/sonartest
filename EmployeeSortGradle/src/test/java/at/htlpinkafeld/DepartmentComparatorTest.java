/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author petro
 */
public class DepartmentComparatorTest {
    
    DepartmentComparator comp;
    
    @Before
    public void setUp() {
        comp = new DepartmentComparator();
    }

    /**
     * Test of compare method, of class DepartmentComparator.
     */
    @Test
    public void testCompare() {
        Employee e1 = new Employee("Hans", "IF", 1234);
        Employee e2 = e1;
        assertEquals(0,comp.compare(e1, e2));
        
        e2 = new Employee("Han", "IE", 1234);
        assertEquals(1, comp.compare(e1,e2));
        
        
    }
    
}

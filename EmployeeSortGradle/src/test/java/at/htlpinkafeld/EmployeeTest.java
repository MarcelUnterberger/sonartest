/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author petro
 */
public class EmployeeTest {
    Employee emp;
    public EmployeeTest() {
        emp = new Employee("hans", "if", 1345);
    }
    
    @Before
    public void setUp() {
    }

    

    /**
     * Test of compareTo method, of class Employee.
     */
    @Test
    public void testCompareTo() {
        Employee retVal;
        retVal = new Employee("hans", "if", 1345);
        assertEquals(0, emp.compareTo(retVal));
        retVal = new Employee("hans", "", 111);
        assertEquals(false, emp.compareTo(retVal) == 0);
        retVal = new Employee("hans", "if", 1344);
        assertEquals(100, emp.compareTo(retVal));
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import at.htlpinkafeld.Employee;
import java.util.Comparator;

/**
 *
 * @author petro
 */
public class DepartmentComparator implements Comparator<Employee>{
    @Override
    public int compare(Employee e1, Employee e2){
        int val =  e1.getName().compareTo(e2.getName());
        int test = 0;
        if(val == 0){
            return e1.compareTo(e2);
        }else return val;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author petro
 */
public class Employee  implements Comparable{
    private String name;
    private String dept;
    private double sal;

    public Employee(String name, String dept, double sal) {
        this.name = name;
        this.dept = dept;
        this.sal = sal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public double getSal() {
        return sal;
    }

    public void setSal(double sal) {
        this.sal = sal;
    }
    @Override
    public int compareTo(Object emp){
        if(emp.getClass() != Employee.class){
            throw new IllegalArgumentException();
        }
        Employee other = (Employee) emp;
        if(this.name.compareTo(other.name) == 0){
            if(this.dept.compareTo(other.dept) == 0){
                return (int)(this.sal*100) -  (int)(other.sal*100);
            }
            return this.dept.compareTo(other.dept);
        }
        return this.name.compareTo(other.name);
    }
    @Override
    public String toString(){
        return name + ", Deparment: " + dept + ", Salary: " + sal;
    }
}

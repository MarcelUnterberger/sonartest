/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import java.util.Comparator;

/**
 *
 * @author petro
 */
public class SalaryComparator implements Comparator<Employee>{
    @Override
    public int compare(Employee e1, Employee e2){
        if(e1.getSal() - e2.getSal() == 0){
            return e1.compareTo(e2);
        }else return (int)(e1.getSal()*100) - (int)(e2.getSal()*100);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.unterberger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author Marcel
 */
@Path(RestApi.PATH)
public class RestApi {
    public static final String PATH = "/restapi";
    
    @Path("hellohtml")
    @GET
    @Produces ("text/html")
    public Response getHelloPlainText(@QueryParam("name") String name) {
        return Response.ok().entity("<HTML> <body> <h1>HTML: Hello " + name
            + "</h1> </body> </HTML>").build();
        
    }
    
    @Path("personjson")
    @GET
    @Produces("application/json")
    public Response getPersonJSON(@QueryParam("firstName") String fN, @QueryParam("lastName") String lN) {
        return Response.ok().entity(new Person(fN,lN)).build();
    }
    
    @Path("personxml")
    @GET
    @Produces("application/xml")
    public Response getPersonXML(@QueryParam("firstName") String fN, @QueryParam("lastName") String lN) {
        return Response.ok().entity(new Person(fN,lN)).build();
    }
    
    
}
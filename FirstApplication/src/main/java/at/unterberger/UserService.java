/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.unterberger;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Marcel
 */
@Path(UserService.PATH)
public class UserService {

    public static final String PATH = "users";
    @Context
    private ServletContext context;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getUsers() {
        try {
            return Response.ok().entity(UserSingleton.getInstance(context).getUsers()).build();
        } catch (IOException ex) {
            System.out.println("Exception: " + ex.toString());
            return Response.serverError().entity(ex).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getUser(@PathParam("id") int id) {
        try {
            Object user =  UserSingleton.getInstance(context).getUser(id);
            if(user != null) {
                return Response.ok().entity(user).build();
            }
            
            return Response.serverError().build();
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity(ex).build();        
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response putUser(@PathParam("id") int id, User user) {
        try {
            boolean updated = UserSingleton.getInstance(context).updateUser(user);
            if(updated) {
                
                return Response.ok().entity(user).build();
            }
            return Response.status(501).build();
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity(ex).build();
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postUser(User user) {
        try {
            UserSingleton.getInstance(context).createUser(user);
            return Response.ok().build();
        }catch (IOException ex) {
            return Response.serverError().entity(ex).build();    
        }
    }
    
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)

    public Response deleteUser(@PathParam("id") int id) {
        try {
            boolean deleted = UserSingleton.getInstance(context).deleteUser(id);
            if(deleted) {
                return Response.ok().build();
            }
            
            return Response.status(502).build();
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity(ex).build();
        }
    }

}

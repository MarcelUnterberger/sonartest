/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.unterberger;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;

/**
 *
 * @author Marcel
 */
public class UserSingleton {
    
    
    private Map userMap;
    private static UserSingleton INSTANCE = null;
    private int userNumber = 0;
    private ServletContext context;
    boolean loaded = false;
    
    private UserSingleton(ServletContext context) {
        this.context = context;
    }
    
    public static UserSingleton getInstance(ServletContext context) {
        if(INSTANCE == null) {
            INSTANCE = new UserSingleton(context);
        }
        return INSTANCE;
    }

    public Object getUser(int id) throws IOException{
        if(!loaded) {
            this.loadResource();
        }
        return userMap.get(id);
    }
    
    public Object[] getUsers() throws IOException{
        if(!loaded) {
            this.loadResource();
        }
        return userMap.values().toArray();
    }
    
    public void createUser(User user) throws IOException{
        if(!loaded) {
            this.loadResource();
        }
        ++userNumber;
        user.setId(userNumber);    
        userMap.put(userNumber, user);
    }
    
    public boolean updateUser(User user) throws IOException{
        if(!loaded) {
            this.loadResource();
        }
        if(userMap.containsKey(user.getId())) {
            userMap.put(user.getId(), user);
            return true;
        }
        
        return false;
    }
    
    public boolean deleteUser(int id) throws IOException{
        if(!loaded) {
            this.loadResource();
        }
        
        if(this.getUser(id) != null) {
            userMap.remove(id);
            return true;
        }
        
        return false;
    }    
            
    public void loadResource() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        userMap = new HashMap();
        
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("data.json");
        
        
        User[] userArray = mapper.readValue(stream , User[].class);
        
        for(User u : userArray) {
            userMap.put(u.getId(), u);
            userNumber++;
        }
        loaded = true;
    }
}
